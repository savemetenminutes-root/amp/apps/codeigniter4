#!/bin/bash

cd /var/www/content/codeigniter4

ssh-keyscan -H github.com >> /root/.ssh/known_hosts
php -d memory_limi=-1 -d max_execution_time=0 ../composer.phar install --no-interaction
